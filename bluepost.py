import requests
from rss_parser import Parser
import json
import re


def writeOut(response):
    xml = response
    # data = []
    # with open('data.json', 'w') as jsn:
    #     json.dump(data, jsn)

    with open('data.json', 'r') as jsn:
        posted = json.load(jsn)



    # Limit feed output to 5 items
    # To disable limit simply do not provide the argument or use None
    parser = Parser(xml=xml.content, limit=4)
    feed = parser.parse()

    # Print out feed meta data
    link = []
    i = 0
    # Iteratively print feed items
    for item in feed.feed:
        postLinkNumber = re.search(r'\d+', item.link)
        if not posted.__contains__(postLinkNumber.group()):  # mozno prepsat na ukladani odkazu
            posted[i] = postLinkNumber.group()
            if not re.search(r"[us]{2}", item.link) and not re.search(r"topic", item.link):
                link.append(item.link)
            i += 1
        else:
            i += 1
            continue



    with open('data.json', 'w') as jsn:
        # data = {'posted': [posted]}
        json.dump(posted, jsn)

    return link


# print(writeOut(requests.get("https://www.wowhead.com/blue-tracker?rss")))
