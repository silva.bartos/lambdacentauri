import json
import requests
import re
import discord
import asyncio
import contextlib
from datetime import datetime
from datetime import timedelta
import urllib.request

from discord.ext import commands
from discord.ext import tasks

#lokalni knihovny
import bluepost


client = discord.Client()
bot = commands.Bot(command_prefix='$')
apiPath = "https://raider.io/api/v1/characters/profile?region=eu&realm=drakthul&name="
raiderIOpath = "https://raider.io"


dungs = {
    "DOS": "#de-other-side",
    "other": "#de-other-side",
    "HOA": "#halls-of-atonement",
    "halls": "#halls-of-atonement",
    "MOTS": "#mists-of-tirna-scithe",
    "mists": "#mists-of-tirna-scithe",
    "PF": "#plaguefall",
    "plague": "#plaguefall",
    "SD": "#sanguine-depths",
    "sanguine": "#sanguine-depths",
    "SOA": "#spires-of-ascension",
    "spires": "#spires-of-ascension",
    "NW": "#the-necrotic-wake",
    "necrotic": "#the-necrotic-wake",
    "TOP": "#theater-of-pain",
    "theater": "#theater-of-pain",
}


@bot.event
async def on_ready():
    print('We have logged in as {0.user}'.format(bot))
    print('Connected to servers:')
    for guild in bot.guilds:
        print(guild.name)


@bot.command(name='rio', help='$rio [characterName] {requests raiderIO score}')
async def rio(ctx, arg):
    response = requests.get(
        "https://raider.io/api/v1/characters/profile?region=eu&realm=drakthul&name=" + arg + "&fields=mythic_plus_scores_by_season%3Acurrent")
    if response.status_code == 200:
        content = json.loads(response.text)
        await ctx.send("Character **{0}** has **{1}** raiderIO score.".format(content["name"],
                                                                              content["mythic_plus_scores_by_season"][
                                                                                  0]["scores"]["all"]))
    else:
        await ctx.send("Character with name **{0}** doesn't exist on Drak'thul.".format(arg))


@bot.command()
async def ping(ctx):
    await ctx.send("pong!")


@bot.command(name='routes', help='$routes [shortDungeonName] {Plaguefall - PF/plague}')
async def routes(ctx, arg):
    if arg in dungs:
        req = urllib.request.Request("https://raider.io/news?tag=weekly-route", headers={'User-Agent': "Magic Browser"})
        con = urllib.request.urlopen(req)
        page = con.read()
        regex = r"\"\/news\/.*?\""
        matches = re.finditer(regex, str(page), re.MULTILINE)
        final = ""
        for matchNum, match in enumerate(matches, start=1):
            final = match.group()
            break
        print(raiderIOpath+final+dungs[arg])
        await ctx.send("{page}{week}{dung}".format(page=raiderIOpath, week=final[1:len(final)-1], dung=dungs[arg]))

    else:
        await ctx.send("Dungeon **{0}** doesn't exist.".format(arg))


@bot.command(name='profile', help='$profile [characterName] {works only on drakthul and burning-blade}')
async def profile(ctx, *args):
    try:
        if len(args) > 0:
            responded = 0
            response = requests.get(
                "https://raider.io/api/v1/characters/profile?region=eu&realm=drakthul&name=" + args[0]
                + "&fields=gear%2Cguild%2Ccovenant%2Craid_progression%2Cmythic_plus_scores_by_season%3Acurrent%2Cmythic_plus_best_runs")

            if response.status_code == 200:
                responded = responded + 1
                embed = charInfo(response)
                await ctx.send(embed=embed)

            responsebb = requests.get(
                "https://raider.io/api/v1/characters/profile?region=eu&realm=burning-blade&name=" + args[0]
                + "&fields=gear%2Cguild%2Ccovenant%2Craid_progression%2Cmythic_plus_scores_by_season%3Acurrent%2Cmythic_plus_best_runs")
            if responsebb.status_code == 200:
                responded = responded + 1
                embed = charInfo(responsebb)
                await ctx.send(embed=embed)

            if responded == 0 and response.status_code < 500:
                await ctx.send("Character with name **{0}** doesn't exist on either Drak'thul or Burning Blade."
                               .format(args[0]))
            elif responded == 0 and response.status_code >= 500:
                await ctx.send("RaiderIO website is not responding.")
        else:
            await ctx.send("format **$profile** ['name'] ")
    except IndexError as i:
        print(i)
        await ctx.send("**$profile** requires argument ")


def charInfo(response):
    content = json.loads(response.text)
    embed = discord.Embed(title="{Pname}-{realm} <{guild}>".format(Pname=content["name"],
                                                                   realm=content["realm"],
                                                                   guild=content["guild"]["name"]),
                          description="{spec} {Class} {gear} [{covenant}-{renown}]".format(Class=content["class"],
                                                                                           spec=content[
                                                                                               "active_spec_name"],
                                                                                           gear=str(content["gear"][
                                                                                                        "item_level_equipped"]),
                                                                                           covenant=content["covenant"][
                                                                                               "name"],
                                                                                           renown=content["covenant"][
                                                                                               "renown_level"]),
                          color=0x00ff00)
    embed.add_field(name="RIO", value=content["mythic_plus_scores_by_season"][0]["scores"]["all"], inline=True)
    if len(content["mythic_plus_best_runs"]) > 0:
        embed.add_field(name="Best run",
                        value=("{dg1}-{dg1Key}".format(dg1=content["mythic_plus_best_runs"][0]["short_name"],
                                                       dg1Key=content["mythic_plus_best_runs"][0]["mythic_level"])),
                        inline=True)
    else:
        embed.add_field(name="Best runs", value="none")

    embed.add_field(name="Raid progress",
                    value=content["raid_progression"]["castle-nathria"]["summary"],
                    inline=True)
    embed.set_thumbnail(url=content["thumbnail_url"])
    return embed

@tasks.loop(minutes=15) #pro intervaly
async def bluePost():
    await bot.wait_until_ready()
    bluePostLinks = bluepost.writeOut(response=requests.get("https://www.wowhead.com/blue-tracker?rss"))
    channel = bot.get_channel(415403489058095104)
    for postLink in bluePostLinks:
        await channel.send(postLink)


def setup(bot):
    # bot.add_extension("test")
    bot.add_command(help)
    bot.add_command(rio)
    bot.add_command(ping)


token = open("token.txt")
bluePost.start()
bot.run(token.read())

# 0auth -- https://discord.com/api/oauth2/authorize?client_id=794861747559202857&permissions=2148006976&scope=bot